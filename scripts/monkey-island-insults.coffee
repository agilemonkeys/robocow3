# Description:
#   After triggered, Robocow insults an user with a Monkey Island insult.
#
# Commands:
#   hubot insult - Randomly sends an insult.
#   hubot insult like a sir - Randomly sends an insult as they should be done.
#   hubot fight <insult> - Insult hubot with a monkey island insult and see how she replies.

insults = [
  "¡Llevarás mi espada como si fueras un pincho moruno!"
  "¡Luchas como un granjero!"
  "¡No hay palabras para describir lo asqueroso que eres!"
  "¡He hablado con simios más educados que tu!"
  "¡Obtuve esta cicatriz en mi cara en una lucha a muerte!"
  "¡No pienso aguantar tu insolencia aquí sentado!"
  "¡Mi pañuelo limpiará tu sangre!"
  "¡Ha llegado tu HORA, palurdo de ocho patas!"
  "¿Has dejado ya de usar pañales?"
  "¡Una vez tuve un perro más listo que tú!"
  "¡Nadie me ha sacado sangre jamás, y nadie lo hará!"
  "¡Me das ganas de vomitar!"
  "¡Tienes los modales de un mendigo!"
  "¡He oído que eres un soplón despreciable!"
  "¡La gente cae a mis pies al verme llegar!"
  "¡Demasiado bobo para mi nivel de inteligencia!"
  "Los enemigos contra los que me enfrenté fueron aniquilados."
  "Eres tan repulsivo como una mona marrana."
  "¡Que el cielo conserve mi vista! ¡Pareces muerto como el pescado!"
  "¡Te perseguiré día y noche sin ningún respeto!"
  "¡Voy a ensartarte como a una puerca guarrería!"
  "Mis grandes hazañas por todo el Caribe son celebradas."
  "Jamás había visto a nadie tan torpe con la espada"
  "¡No descansaré hasta que hayas sido exterminado!"
  "¡Matarte sería un homicidio justificado!"
  "¡Si luchas cara a cara conmigo necesitarás un consejero!"
  "Te dejaré devastado, mutilado y perforado."
  "¡Eres el monstruo más feo jamás creado!"
  "¿Te gustaría ser enterrado o incinerado?"
  "Cuando tu padre te vio por primera vez debió sentirse mortificado."
  "¡Mis graciosos comentarios no igualarás con acierto!"
  "¡En guardia! ¡Mamarracho!"
]

insults_master = [
  "Mi lengua es más hábil que cualquier espada."
  "¡Ordeñaré hasta la última gota de sangre de tu cuerpo!"
  "Ya no hay técnicas que te puedan salvar."
  "Ahora entiendo lo que significan basura y estupidez."
  "¡Obtuve esta cicatriz en mi cara en una lucha a muerte!"
  "¡Eres como un dolor en la parte baja de la espalda!"
  "Mi nombre es reconocido en cada sucio rincón de esta isla."
  "Hoy te tengo preparada una larga y dura lección."
  "Espero que tengas un barco para una rápida huida."
  "Sólo he conocido a uno tan cobarde como tú."
  "Nunca me verán luchar tan mal como tú lo haces."
  "Si tu hermano es como tú, mejor casarse con un cerdo."
  "Cada palabra que sale de tu boca es una estupidez."
  "Mi espada es famosa en todo el Caribe - ¡Mi nombre es temido en cada sucio rincón de esta isla!"
  "Mis enemigos más sabios corren al verme llegar - Veo gente como tú arrastrándose por el suelo de los bares."
  "¡Tengo el coraje y la técnica de un maestro!"
  "Mis ataques han dejado islas enteras sin poblados."
  "Tienes el atractivo de una iguana."
  "Nada podrá salvar tu pobre pellejo resecado."
  "¡Persiguiendo a mi presa, la dejo en un aprieto!"
  "Tus labios me recuerdan a los del pescado del día."
  "Mis habilidades con la espada son muy veneradas."
  "Nunca he perdido una pelea a espada."
  "¡Tu olor hasta a un limpiador de retretes dejaría conmocionado!"
  "Cuando termine, tu cuerpo estará podrido y disecado."
  "Nunca antes me había encarado a alguien tan pordiosero."
  "No sé cuál de mis características te tiene más intimidado."
  "Tu cara haría vomitar hasta al cerdo menos aseado."
  "Te dejaré elegir. ¡Puedes ser destripado o decapitado!"
  "Eres de un linaje sucio y alocado."
  "Nada podrá impedirme que te arrastre como el viento."
  "¡Tu madre es un marimacho!"
]

answers = [
  "Primero deberías dejar de usarla como un plumero."
  "Qué apropiado, tú peleas como una vaca."
  "Sí que las hay, sólo que nunca las has aprendido."
  "Me alegra que asistieras a tu reunión familiar diaria."
  "Espero que ya hayas aprendido a no tocarte la nariz."
  "Ya te están fastidiando otra vez las almorranas, ¿eh?"
  "Ah, ¿ya has obtenido ese trabajo de barrendero?"
  "Y yo tengo un SALUDO para ti, ¿te enteras?"
  "¿Por qué? ¿Acaso querías pedir uno prestado?"
  "Te habrá enseñado todo lo que sabes."
  "¿TAN rápido corres?"
  "Me haces pensar que alguien ya lo ha hecho."
  "Quería asegurarme de que estuvieras a gusto conmigo."
  "Qué pena me da que nadie haya oído hablar de ti."
  "¿Incluso antes de que huelan tu aliento?"
  "Estaría acabado si la usases alguna vez."
  "Con tu aliento, seguro que todos fueron asfixiados."
  "¿Es que TANTO me parezco a tu hermana?"
  "La única forma en la que te conservarás será disecado."
  "Entonces sé un buen perro. ¡Siéntate! ¡Quieto!"
  "Cuando acabe CONTIGO, serás un bistec con disentería."
  "Qué pena que todas estén inventadas."
  "Así habría sido, pero siempre estabas de escapada."
  "Entonces deberías cambiar a descafeinado."
  "Entonces matarte debe ser un fungicidio justificado."
  "Es esa tu cara, pensé que era tu trasero."
  "Tu solo olor me deja enfadado, agitado y enojado."
  "Si no cuentas todas las demás con las que te has citado."
  "Contigo alrededor, preferiría ser fumigado."
  "Al menos el mío puede ser identificado."
  "Podré, si utilizas un spray contra tu mal aliento."
  "¡Tus palabras no son de un macho!"
]

module.exports = (robot) ->
  robot.respond /INSULT$/i, (msg) ->
    random = Math.round(Math.random()*(insults.length-1))
    msg.send insults[random]

  robot.respond /INSULT LIKE A SIR$/i, (msg) ->
    random = Math.round(Math.random()*(insults_master.length-1))
    msg.send insults_master[random]

  robot.respond /FIGHT (.+)$/i, (msg) ->
    index = insults.indexOf(msg.match[1])
    index = insults_master.indexOf(msg.match[1]) if index < 0

    if index >= 0
      msg.send answers[index]
    else
      msg.send "Eso ha sido muy doloroso, me has dejado sin palabras :("